const BN = require('bn.js');
const numStringToBytes32 = function (num) {
  var bn = new BN(num).toTwos(256);
  var n = bn.toString(16);
  while (n.length < 64) {
    n = "0" + n;
  }
  return n;
}
module.exports.numStringToBytes32 = numStringToBytes32;

