const grid = require('./gridCreator');
const manfPubKey = require('../../helpers/pubKey').pubKey;
const keccak256 = require('keccak256');
const helper = require('../../helpers/helpers');

App = {
  web3Provider: null,
  contracts: {},
  myCurrentAccount: '0x0',
  customer: '0x0',
  manufacturer: '0x0',
  distributionCompany: '0x0',
  distributor: '0x0',
  myAccounts: ['0x7b71b7b778cc8b10ac2e30a67872602b1642f051', // ganache accounts
    '0x58b1a8a64b5d94118ead9c586aca76b0f5690041',
    '0xe3bd1db68e5aa30cded83a09da63ed0ab251b548',
    '0x02ba08fd8ebcaa5fafc1570db147b1fad8d8665f',
    '0xb083a23f8c76d745a34e42e01113530deae333f8'],
  init: function () {
    return App.initWeb3();
  },

  // initiate the web3 provider
  initWeb3: function () {
    if (typeof web3 !== 'undefined') {
      // If a web3 instance is already provided by Meta Mask.
      App.web3Provider = web3.currentProvider;
      web3 = new Web3(web3.currentProvider);
    } else {
      // Specify default instance if no web3 instance provided
      App.web3Provider = new Web3.providers.HttpProvider('http://127.0.0.1:7545');
      web3 = new Web3(App.web3Provider);
    }
    return App.initContract();
  },

  // load the contract data from the json file, created when the contract is compiled
  initContract: function () {
    $.getJSON('../contracts/CryptoChip.json', function (CryptoChip) {
      // Instantiate a new truffle contract from the artifact
      App.contracts.CryptoChip = TruffleContract(CryptoChip);
      // Connect provider to interact with contract
      App.contracts.CryptoChip.setProvider(App.web3Provider);
      // App.listenForEvents();
      return App.render();
    });
    App.manufacturer = App.myAccounts[4];
    App.distributionCompany = App.myAccounts[3];
    App.distributor = App.myAccounts[2];
    App.customer = App.myAccounts[1];
  },

  // create the datagrid views and filled them the contract data
  render: function () {
    let cryptochipInstance;
    // Load account data
    web3.eth.getCoinbase(function (err, account) {
      if (err === null) {
        App.myCurrentAccount = account;
      }
    });

    // Load contract data
    App.contracts.CryptoChip.deployed().then(function (instance) {
      cryptochipInstance = instance;
      return cryptochipInstance.ordersCount();
    }).then(function (ordersCount) {
      // Store all promised to get chip info
      const cryptochipPromises = [];
      let tr = '';
      const ordersData = [];
      let chipsData = [];
      let orderObj = {};
      let completeData = '';
      let i = 0;
      for (i = 1; i <= ordersCount; i++) {
        const index = App.customer + helper.numStringToBytes32(i);
        cryptochipPromises.push(cryptochipInstance.orders('0x' + keccak256(index).toString('hex')));
      }
      Promise.all(cryptochipPromises).then((orders) => {
        if (orders.length > 0) {
          orders.forEach(order => {
            let index = App.customer + helper.numStringToBytes32(order[0].toNumber());
            const orderSerials = cryptochipInstance.getSerials('0x' + keccak256(index).toString('hex'));
            promise = Promise.resolve(orderSerials);
            promise.then(function (orderSerialsValue) {
              if (orderSerialsValue.length > 0) {
                const chips = [];
                for (i = 0; i < orderSerialsValue.length; i++) {
                  index = '0x' + helper.numStringToBytes32(order[0].toNumber()) +
                      helper.numStringToBytes32(orderSerialsValue[i].toNumber());
                  const indexHash = parseInt(keccak256(index).toString('hex'), 16);
                  chips.push(cryptochipInstance.chips(indexHash));
                }
                promise = Promise.all(chips);
                promise.then((chipsValues) => {
                  for (i = 0; i < chipsValues.length; i++) {
                    tr = {
                      'orderId': chipsValues[i][0].toNumber(),
                      'serialNumber': chipsValues[i][1].toNumber(),
                      'chipName': chipsValues[i][2],
                      'cloned': chipsValues[i][3],
                      'pubKeyX': chipsValues[i][4],
                      'pubKeyY': chipsValues[i][5],
                    };
                    chipsData.push(tr);
                  }
                  orderObj = {
                    'orderId': order[0].toNumber(),
                    'chipName': order[1],
                    'customerName': order[2],
                    'chips': chipsData,
                  };
                  ordersData.push(orderObj);
                  completeData = JSON.stringify(ordersData);
                  // create the grid with the correct priviliges for the customer.
                  if (App.myCurrentAccount === App.customer) {
                    grid.createGridColomns(true, false, false, true, false,
                      false, true, completeData);
                    // create the grid with the correct priviliges for the manufacturer.
                  } else if (App.myCurrentAccount === App.manufacturer) {
                    grid.createGridColomns(false, false, true, true, false, true,
                      false, completeData);
                    // create the grid with the correct priviliges for the distribution company.
                  } else if (App.myCurrentAccount === App.distributionCompany) {
                    grid.createGridColomns(false, false, false, true, false, false,
                      true, completeData);
                  } else {
                    // create the grid with the correct priviliges for the distributor.
                    grid.createGridColomns(false, false, false, true, false, false,
                      true, completeData);
                  }
                  chipsData = [];
                });
              } else {
                orderObj = {
                  'orderId': order[0].toNumber(),
                  'chipName': order[1],
                  'customerName': order[2],
                  'chips': [],
                };
                ordersData.push(orderObj);
                completeData = JSON.stringify(ordersData);
                // create the grid with the correct priviliges for the customer.
                if (App.myCurrentAccount === App.customer) {
                  grid.createGridColomns(true, false, false, true, false, false,
                    true, completeData);
                  // create the grid with the correct priviliges for the manufacturer.
                } else if (App.myCurrentAccount === App.manufacturer) {
                  grid.createGridColomns(false, false, true, true, false, true,
                    false, completeData);
                  // create the grid with the correct priviliges for the distribution company.
                } else if (App.myCurrentAccount === App.distributionCompany) {
                  grid.createGridColomns(false, false, false, true, false,
                    false, true, completeData);
                } else {
                  // create the grid with the correct priviliges for the distributor.
                  grid.createGridColomns(false, false, false, true, false, false,
                    true, completeData);
                }
              }
            });
          });
        } else {
          // create the grid with the correct priviliges for the customer.
          if (App.myCurrentAccount === App.customer) {
            grid.createGridColomns(true, false, false, true, false, false, true, []);
            // create the grid with the correct priviliges for the manufacturer.
          } else if (App.myCurrentAccount === App.manufacturer) {
            grid.createGridColomns(false, false, true, true, false, true, false, []);
            // create the grid with the correct priviliges for the distribution company.
          } else if (App.myCurrentAccount === App.distributionCompany) {
            grid.createGridColomns(false, false, false, true, false, false, true, []);
          } else {
            // create the grid with the correct priviliges for the distributor.
            grid.createGridColomns(false, false, false, true, false, false, true, []);
          }
        }
      });
    });
  },
  // send the orders that has been initiated by the customer
  addOrder: function (orderData) {
    const index = App.customer + helper.numStringToBytes32(orderData.orderId);
    const orderIndex = '0x' + keccak256(index).toString('hex');
    App.contracts.CryptoChip.deployed().then(function (instance) {
      return instance.addOrder(orderData.orderId, orderData.chipName, orderIndex);
    }).then(function (result) {
    }).catch(function (err) {
    });
  },

  // add the chips that is associated to a given order
  addChip: function (chipData, customerName, orderId) {
    let index = App.customer + helper.numStringToBytes32(orderId);
    const orderIndex = '0x' + keccak256(index).toString('hex');
    const serials = [];
    const chipNames = [];
    const cloned = [];
    const tokenIds = [];
    for (let i = 0; i < chipData.length; i++) {
      index = '0x' + helper.numStringToBytes32(Number(chipData[i].orderId)) +
      helper.numStringToBytes32(Number(chipData[i].serialNumber));
      const indexHash = parseInt(keccak256(index).toString('hex'), 16);
      serials.push(Number(chipData[i].serialNumber));
      chipNames.push(chipData[i].chipName);
      cloned.push(chipData[i].cloned);
      tokenIds.push(indexHash);
    }
    App.contracts.CryptoChip.deployed().then(function (instance) {
      return instance.addChip(orderId, serials, chipNames, '0x' + manfPubKey.x,
        '0x' + manfPubKey.y, cloned,
        App.manufacturer, App.distributionCompany, tokenIds, orderIndex);
    }).then(function (result) {
    }).catch(function (err) {
    });
  },

  setChipClonedField: function (chipData, customerName) {
    const serials = [];
    const cloned = [];
    let nextAddr = '';
    const tokenIds = [];
    for (let i = 0; i < chipData.length; i++) {
      const index = '0x' + helper.numStringToBytes32(Number(chipData[i].orderId)) +
      helper.numStringToBytes32(Number(chipData[i].serialNumber));
      const indexHash = parseInt(keccak256(index).toString('hex'), 16);
      serials.push(Number(chipData[i].serialNumber));
      cloned.push(chipData[i].cloned);
      tokenIds.push(indexHash);
    }
    nextAddr = App.myAccounts[App.myAccounts.indexOf(App.myCurrentAccount) - 1];
    App.contracts.CryptoChip.deployed().then(function (instance) {
      return instance.setChipClonedField(serials, cloned,
        nextAddr, customerName, tokenIds);
    }).then(function (result) {
    }).catch(function (err) {
    });
  },
};
// start executing the program
$(function () {
  $(window).load(function () {
    App.init();
  });
});
