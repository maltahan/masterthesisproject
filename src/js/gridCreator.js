const verifyChip = require('./verifyChip');

// create the datagridview for the orders
const createGridColomns = function (orderAddEnable, orderEditEnable,
  chipAddEnable, chipUpdateEnable, chipDeleteEnable,
  chipColomnsEnable, chipClonEnable, data) {
  let demoData = [];
  let orders = [];
  if (data.length === 0) {
    demoData = [{
      'orderId':1,
      'chipName':'DS28E38',
      'customerName':'0xaaaa',
      'chips':[{
        'orderId': 1,
        'chipName': 'DS28E38',
        'serialNumber': 123456,
        'cloned':  false,
      }],
    }];
    orders = new DevExpress.data.ArrayStore(demoData);
  } else {
    orders = new DevExpress.data.ArrayStore(JSON.parse(data));
  }
  const chipData = [];
  $('#orderList').dxDataGrid({
    dataSource: orders,
    allowColumnResizing: true,
    columnResizingMode: 'nextColumn',
    columnMinWidth: 50,
    allowColumnReordering: true,
    showRowLines: true,
    columnAutoWidth: true,
    wordWrapEnabled: true,
    expandedRowKeys: [1],
    searchPanel: {
      visible: true,
    },
    headerFilter: {
      visible: true,
    },
    paging: {
      enabled: true,
      pageSize: 10,
    },
    pager: {
      showPageSizeSelector: true,
      allowedPageSizes: [5, 10, 20],
      showNavigationButtons: true,
      showInfo: true,
    },
    editing: {
      mode: 'row',
      allowAdding: orderAddEnable,
      allowUpdating: orderEditEnable,
      allowDeleting: orderEditEnable,
    },
    columns: [
      {
        dataField: 'orderId',
        caption: 'orderId',
      },
      {
        dataField: 'chipName',
        caption: 'chipName',
      },
      {
        dataField: 'customerName',
        caption: 'customerName',
        dataType: 'string',
      },
    ], // create the child datagridview for the chips
    masterDetail: {
      enabled: true,
      template: function (container, options) {
        const currentOrderData = options.data;
        $('<div>')
          .addClass('master-detail-caption')
          .text(currentOrderData.chipName + "'s Chips details")
          .appendTo(container);

        $('<div>')
          .addClass('transButton')
          .dxDataGrid({
            allowColumnResizing: true,
            columnResizingMode: 'nextColumn',
            columnMinWidth: 50,
            allowColumnReordering: true,
            showRowLines: true,
            columnAutoWidth: true,
            wordWrapEnabled: true,
            expandedRowKeys: [1],
            searchPanel: {
              visible: true,
            },
            headerFilter: {
              visible: true,
            },
            paging: {
              enabled: true,
              pageSize: 10,
            },
            pager: {
              showPageSizeSelector: true,
              allowedPageSizes: [5, 10, 20],
              showInfo: true,
            },
            editing: {
              mode: 'row',
              allowAdding: chipAddEnable,
              allowUpdating: chipUpdateEnable,
              allowDeleting: chipDeleteEnable,
            },
            columns: [
              {
                dataField: 'chipName',
                caption: 'chipName',
                allowEditing: chipColomnsEnable,
              },
              {
                dataField: 'serialNumber',
                caption: 'serialNumber',
                allowEditing: chipColomnsEnable,
              },
              {
                dataField: 'cloned',
                caption: 'cloned',
                dataType: 'boolean',
                allowEditing: chipClonEnable,
              },
            ],
            onInitNewRow: function (e) {
              e.data.orderId = currentOrderData.orderId;
              e.data.chipName = currentOrderData.chipName;
              e.data.serialNumber = '12345';
              e.data.cloned = false;
            },
            onRowInserting: function (e) {
              chipData.push(e.data);
            },
            dataSource: currentOrderData.chips,
          }).appendTo(container);

        $('<div>').dxButton({
          type: 'success',
          icon: 'fa fa-paper-plane',
          text: 'submit the transaction',
          onClick: function (res) {
            const result = DevExpress.ui.dialog.confirm('Are you sure?', 'Confirm transaction');
            result.done(function (dialogResult) {
              if (dialogResult) {
                if (App.myCurrentAccount.toUpperCase() === App.manufacturer.toUpperCase()) {
                  App.addChip(currentOrderData.chips,
                    currentOrderData.customerName, currentOrderData.orderId);
                } else {
                  App.setChipClonedField(currentOrderData.chips,
                    currentOrderData.customerName);
                }
              }
            });
          },
        }).appendTo(container);
        if (App.myCurrentAccount.toUpperCase() !== App.manufacturer.toUpperCase()) {
          $('<div>').dxButton({
            type: 'danger',
            icon: 'check',
            text: 'cloning checker',
            onClick: function (res) {
              // check whether the chip is cloned or not
              verifyChip.verify(currentOrderData.chips[0]);
            },
          }).appendTo(container);
        }
      },
    },
    onInitNewRow: function (e) {
      e.data.orderId = 1;
      e.data.chipName = 'DS28E38';
      e.data.customerName = App.myCurrentAccount;
    },
    onRowInserting: function (e) {
      App.addOrder(e.data);
    },
  });
};


module.exports.createGridColomns = createGridColomns;
