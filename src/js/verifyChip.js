const sha256 = require('js-sha256');
const EC = require('elliptic').ec;

const ec = new EC('p256');

function hexToBytes(hex) {
  const bytes = [];
  for (let c = 0; c < hex.length; c += 2) {
    bytes.push(parseInt(hex.substr(c, 2), 16));
  }
  return bytes;
}
const verify = function (chipKey) {
  let statusMessage = '';
  $.ajax({
    url: 'http://192.168.137.252',
    method: 'GET',
    dataType: 'json',
  }).done(function (chipData) {
    if (chipData.message !== 'false') {
      const manufacturerPublicKey = chipKey.pubKeyX.substr(2) + chipKey.pubKeyY.substr(2);
      const pub = '04' + manufacturerPublicKey;
      const pubkey = ec.keyFromPublic(pub, 'hex');
      const romId = chipData.romId;
      const pageData = chipData.page_data;
      const challenge = chipData.challenge;
      const pageNum = '00';
      const manId = chipData.manId;
      const message = hexToBytes(romId + pageData + challenge + pageNum + manId);
      const signature = {
        r: chipData.r,
        s: chipData.s,
      };
      const hash = sha256.create();
      hash.update(message);
      const msgHash = hash.hex();
      const valid = pubkey.verify(msgHash, signature);
      // check whether the signature valid or not against the stored public key.
      if (!valid) {
        statusMessage = 'the chip has been cloned';
        const myDialog = DevExpress.ui.dialog.custom({
          title: 'cloning checker',
          message: statusMessage,
          buttons: [{
            text: 'OK',
            onClick: function (e) {
            },
          },
          ],
        });
        myDialog.show().done(function (dialogResult) {
        });
      } else {
        statusMessage = 'the chip is OK';
        const myDialog = DevExpress.ui.dialog.custom({
          title: 'cloning checker',
          message: statusMessage,
          buttons: [{
            text: 'OK',
            onClick: function (e) {
            },
          },
          ],
        });
        myDialog.show().done(function (dialogResult) {
        });
      }
    } else {
      verify(chipKey);
    }
  }).fail(function (xhr, textStatus, errorThrown) {
  });
};

module.exports.verify = verify;
