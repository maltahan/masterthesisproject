var cryptoChip = artifacts.require('./CryptoChip.sol');

module.exports = function (deployer) {
  deployer.deploy(cryptoChip, "DSERCToken", "DST", "0xb083A23F8C76d745A34e42e01113530Deae333F8", "DSTUri");
}
