# Admin Dashboard to check the counterfeit of a chip
**this Dashboard** is a responsive Bootstrap Admin website. It provides you with a capabilities of
of adding an order of a number of chips and with blockchain mechanisms to check, whether these 
chips are counterfeit or not. beside of course the ability to submit the transaction to 
the blockchain.
# Preview

### Screenshot

![admin dashboard website preview](https://gitlab.com/maltahan/masterthesisproject/raw/master/screenshot.jpg)


## TOC
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installing & Local Development](#installing--local-development)
- [Files/Folder Structure](#filesfolders-structure)
- [Deployment](#deployment)
- [Built With](#built-with)
- [Authors](#authors)
- [License](#license)


## Getting Started
In order to run **this Dashboard** on your local machine all what you need to do is to have the prerequisites stated below installed on your machine and follow the installation steps down below.

#### Prerequisites
  - Node.js
  - Yarn or NPM
  - Git
  - libpng-dev *linux only*

#### Installing & Local Development
Start by typing the following commands in your terminal in order to get **Dashboard** full package on your machine and starting a local development server with live reload feature.

```
> git clone https://gitlab.com/maltahan/masterthesisproject
> cd masterthesisproject
> npm install
> npm run dev or npm start
```


## Files/Folders Structure
Here is a brief explanation of the website folder structure and some of its main files usage:

```

└── build
    └── contract                # Contains all smart files info after migrate them with truffle.
└── contracts                   # Contains all smart contract classes and interfaces.
└── migration                   # Contains configuartion about smart contracts migration.
└── src                         # Contains all website source files.
│   └── assets                  # Contains JS, CSS, images and icon fonts.
│   │   └── scripts             # Contains all JavaScript files.
│   │   │   └── constants       # website constant values like color values.
│   │   │   └── masonry         # Masonry layout code.
│   │   │   └── index.js        # Indicator file.
│   │   │
│   │   └── static              # Contains the non-code files.
│   │   │   └── fonts           # Contains icon fonts.
│   │   │   └── images          # Contains all website images/svg.
│   │   │
│   │   └── styles              # Contains all SCSS files.
│   │       └── spec            # Contains custom SCSS files.
│   │       │   └── components  # Contains all website components.
│   │       │   └── generic     # Contains basic scaffolding styles.
│   │       │   └── screens     # Contains views specific styles.
│   │       │   └── settings    # Contains all website variables.
│   │       │   └── tools       # Contains all mixins.
│   │       │   └── utils       # Contains helper classes.
│   │       │   └── index.scss  # Indicator file.
│   │       │
│   │       └── vendor          # Contains all plugin files & custom styles.
│   │       └── index.scss      # Indicator file.
│   │
│   └── *.html                  # All HTML pages files .
│   └── contracts               # contains the smart contract transaction file.
│   └── css                     # contains costume css for the datagrids.
│   └── dist                    # contains the bundle.js file where all node dependencies are composed in it using browserify tool.
│   └── js                      # contains all the js that interact with the blockchain.
│       └── app.js              # contains the main logic for the website.
│       └── gridCreator.js      # contains the logic for creating the orders and transaction gridviews.
│       └── verifyChip.js       # contains the logic to check the validity of the signature, that we get from the DS28E38 chip.
│       └── truffle.js          # contains the logic of truffle framework like (contract migration, test, build,...).
│       └── web3.min.js         # contains the logic to handle the interaction between the website and the smart contract.
│
│
└── test                        # contains some tests to test the smart contract functions.

└── webpack                     # Contains Webpack init code.
│   └── plugins                 # Contains all Webpack plugins config.
│   └── rules                   # Contains Loaders config code.
│   └── config.js               # Contains Webpack config object.
│   └── devServer.js            # Webpack dev server config code.
│   └── manifest.js             # All build system constants.
│
└── package.json                # Package metadata.
└── README.md                   # Manual file.
└── webpack.config.js           # Webpack main config file.
```

## Deployment
In deployment process, you have two commands:

1. Build command
Used to generate the final result of compiling src files into build folder. This can be achieved by running the following command:
```
> npm run build
```

2. Preview command
Used to create a local dev server in order to preview the final output of build process. This can be achieved by running the following command:
```
> npm run preview
```

## Built With
- [Truffle](https://truffleframework.com/)
- [Ganach](https://truffleframework.com/ganache)
- [MetaMask](https://metamask.io/)
- [Web3](https://github.com/ethereum/web3.js/)
- [Babel](https://babeljs.io/)
- [Webpack](https://webpack.js.org/)
- [Eslint](https://eslint.org/)
- [Sass](http://sass-lang.com/)
- [Postcss](http://postcss.org/)
- [Stylelint](https://stylelint.io/)
- [Bootstrap](http://getbootstrap.com/)
- [Jquery](https://jquery.com/)
- [Jquery Sparkline](https://omnipotent.net/jquery.sparkline/)
- [Masonry](https://masonry.desandro.com/)
- [Moment](https://momentjs.com/)
- [Perfect Scrollbar](https://github.com/utatti/perfect-scrollbar)
- [Fontawesome](http://fontawesome.io/)
- [Themify Icons](https://themify.me/themify-icons)


## Changelog
#### V 1.0.0
Initial Release

## Authors
Mohammad Altahan(mohammad.tahan1992@gmail.com)


## License

this website is licensed under The MIT License (MIT). Which means that you can use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the final products. 
But you always need to state that Mohammad Altahan is the original author of this website.
