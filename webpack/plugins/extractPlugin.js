const manifest = require('../manifest');
const extractTextPlugin = require('extract-text-webpack-plugin');

module.exports = new extractTextPlugin({
  filename: manifest.outputFiles.css,
  allChunks: true,
});
