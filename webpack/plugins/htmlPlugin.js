const path = require('path');
const manifest = require('../manifest');
const htmlWebpackPlugin = require('html-webpack-plugin');

const titles = {
  'index': 'Dashboard',
};

module.exports = Object.keys(titles).map(title => {
  return new htmlWebpackPlugin({
    template: path.join(manifest.paths.src, `${title}.html`),
    path: manifest.paths.build,
    filename: `${title}.html`,
    inject: true,
    minify: {
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: true,
      removeComments: true,
      useShortDoctype: true,
    },
  });
});
