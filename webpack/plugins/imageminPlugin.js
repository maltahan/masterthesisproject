const manifest = require('../manifest');
const imageminPlugin = require('imagemin-webpack-plugin').default;

module.exports = new imageminPlugin({
  disable: manifest.IS_DEVELOPMENT,
});
