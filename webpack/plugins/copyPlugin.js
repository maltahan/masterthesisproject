const path = require('path');
const manifest = require('../manifest');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = new CopyWebpackPlugin([
  {
    from : path.join(manifest.paths.src, 'assets/static'),
    to   : path.join(manifest.paths.build, 'assets/static'),
  },
]);
