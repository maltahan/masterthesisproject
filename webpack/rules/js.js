module.exports = {
  enforce: 'pre',
  test    : /\.(js)$/,
  exclude : /(node_modules|build|dist\/)/,
  use: ['babel-loader'],
};
