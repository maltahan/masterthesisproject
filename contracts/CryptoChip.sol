pragma solidity ^0.5.7;

import "./ERC721.sol";
import "./ERC721Metadata.sol";


contract CryptoChip is ERC721, ERC721Metadata {

    // A mapping to Store the Orders.
    mapping(bytes32 => Order) public orders;
    uint256 public ordersCount;
    uint256[] internal emptySerials;
 
    // A mapping to Store the Chips.
    mapping(uint256 => Chip) public chips;
    uint256 public chipsCount;
    
    // The address of the owner(manufacturer)
    address public owner;

    // Store the tokenURI
    string internal tokenURI;

    struct Order {
        uint256 orderId;
        string chipName;
        address customerName;
        uint256[] chipSerials;
    }

    struct Chip {
        uint256 orderId;
        uint256 serialNumber;
        string chipName;
        bool cloned;
        bytes32 publicKeyX;
        bytes32 publicKeyY;
    }
    
    /**
     * @dev Constructor function
     */
    constructor(string memory _name, string memory _symbol, address manufacturerAddr, string memory _tokenURI)
    public ERC721Metadata(_name, _symbol) {
        owner = manufacturerAddr;
        tokenURI = _tokenURI;
    }

    /**
     * @dev Add new order by a customer to buy some chips.
     * @param orderId uint256 the id of the order.
     * @param chipName string the name of the chip.
     * @param orderIndex bytes32 the index of the order.
     */
    function addOrder(uint256 orderId, string memory chipName, bytes32 orderIndex) public {
        ordersCount ++;
        orders[orderIndex] = Order(orderId, chipName, msg.sender, emptySerials);
    }

    /**
     * @dev The manufacturer add the chips that the customer requested.
     * Can only be called by the manufacturer.
     * @param orderId uint256 the id of the order.
     * @param serialNumbers uint256[] the serial numbers of the chips.
     * @param chipNames bytes32[] the names of the requested chips.
     * @param publicKeyX bytes32 the x component of the chip PublicKey.
     * @param publicKeyY bytes32 the y component of the chip PublicKey.
     * @param cloned bool[] this field indicated whther the chip is cloned or not.
     * @param _to address the address that we want to create a token for.
     * @param operator address the address that we want to Transfer the ownership of a token to.
     * @param tokenIds uint256[] the list of tokenIds.
     * @param ordIndex bytes32 the index of the order.
     */
    function addChip(uint256 orderId, uint256[] memory serialNumbers, bytes32[] memory chipNames, bytes32 publicKeyX,
    bytes32 publicKeyY, bool[] memory cloned, address _to, 
    address operator, uint256[] memory tokenIds, bytes32 ordIndex) public {
        require(owner == msg.sender, "Not authorized only the manufacturer can add chip.");
        for(uint256 i = 0; i < serialNumbers.length; i++) {
            // Add the chip to the list and use the hash of the serial number and order id as an index.
            chips[tokenIds[i]] = Chip(orderId, serialNumbers[i],
            bytes32ToString(chipNames[i]), cloned[i], publicKeyX, publicKeyY);
            chipsCount++;
            
            // Modify the order by adding the new chips.
            orders[ordIndex].chipSerials.push(serialNumbers[i]);
            
            // Create the new token.
            mintUniqueTokenTo(_to, tokenIds[i], tokenURI);
            
            // Transfer the ownership of the token from the manufacturer to the next enitiy in the Bchain.
            super.safeTransferFrom(owner, operator, tokenIds[i]);
        }
    }
    
    /**
     * @dev Returns the serial numbers of a given order and a given customer.
     * @param orderIndex bytes32 the index of the order.
     */
    function getSerials(bytes32 orderIndex) view public returns (uint256[] memory) {
        if(orders[orderIndex].chipSerials.length == 0) {
            return new uint256[](0);
        } else {
            return orders[orderIndex].chipSerials;
        }
    }

     /**
     * @dev Set the value of the chip cloned field, i.e cloned or not
     * Can only be called by the address how ownes the token of this chip.
     * @param serialNumbers uint256[] the serialNumbers of the chips.
     * @param cloned bool[] this field indicated whther the chip has been cloned or not.
     * @param nxtAddr address the address of the next enitiy in the blockchain, that
       we want to transfer the ownership of a chip to.
       @param tokens uint256[] the list of tokenIds.
     */
    function setChipClonedField(uint256[] memory serialNumbers, bool[] memory cloned,
    address nxtAddr, address customerName, uint256[] memory tokens) public {
        //Only the operator who has the token can change the cloned field.
        for(uint256 i = 0; i < serialNumbers.length; i++) {
            require(super.ownerOf(tokens[i]) == msg.sender,
            "you are not authurized to change the cloned field");

            // If the chip is being cloned then burn its token.
            chips[tokens[i]].cloned = cloned[i];
            if(cloned[i] == true) {
                super._burn(msg.sender, tokens[i]);
            }
            
            // Transfer the ownership of the token to the next address in the blockchain.
            if(msg.sender != customerName && cloned[i] == false) {
                super.safeTransferFrom(msg.sender, nxtAddr, tokens[i]);
            }
        }
    }
    
    /**
     * @dev Sell the chip to another entity
     * Can only be called by the address how owns the token of this chip.
     * @param to address the address of the next enitiy in the blockchain, that want to buy a chip.
     * @param tokenId uint256 the token id of the chip.
     */
    function sellChip(address to, uint256 tokenId) public {
        require(super.ownerOf(tokenId) == msg.sender, "you are not authurized to sell the chip");
        
        // Transfer the ownership of the token to the next address in the blockchain.
        super.safeTransferFrom(msg.sender, to, tokenId);
    }

    function bytes32ToString(bytes32 x) pure internal returns (string memory) {
        bytes memory bytesString = new bytes(32);
        uint charCount = 0;
        for (uint j = 0; j < 32; j++) {
            byte char = byte(bytes32(uint(x) * 2 ** (8 * j)));
            if (char != 0) {
                bytesString[charCount] = char;
                charCount++;
            }
        }
        bytes memory bytesStringTrimmed = new bytes(charCount);
        for (uint256 j = 0; j < charCount; j++) {
            bytesStringTrimmed[j] = bytesString[j];
        }
        return string(bytesStringTrimmed);
    }

    // Mint new tokens.
    function mintUniqueTokenTo(address _to, uint256 _tokenId, string memory tokenURIVal) internal {
        super._mint(_to, _tokenId);
        super._setTokenURI(_tokenId, tokenURIVal);
    }
}
