var cryptoChip = artifacts.require('./CryptoChip.sol')

contract('CryptoChip', function (accounts) {
  var CryptoChipInstance
  it('check that the owner of the contract is the manufacturer', function () {
    return cryptoChip.deployed().then(function (instance) {
      CryptoChipInstance = instance;
      return CryptoChipInstance.owner();
    }).then(function (owner) {
      assert.equal(owner, accounts[4], 'the owner of the contract is the manufacturer')
    })
  });

  it('add a new Order', function () {
    return cryptoChip.deployed().then(function (instance) {
      CryptoChipInstance = instance;
      CryptoChipInstance.addOrder(1, 'DS50', '0xaaaa');
      return CryptoChipInstance.ordersCount();
    }).then(function (ordersCount) {
      assert.equal(ordersCount, 1, 'the order has been added')
    })
  });

  it('throws an exception for invalid orders parameters', function () {
    return cryptoChip.deployed().then(function (instance) {
      CryptoChipInstance = instance;
      return CryptoChipInstance.addOrder(1, 2.5, '0xaaaa');
    }).then(assert.fail).catch(function (error) {
      assert(error.message.indexOf('invalid string value') >= 0, 'error message must contain (invalid string value) phrase');
      return CryptoChipInstance.ordersCount();
    }).then(function (ordersCount) {
      assert.equal(ordersCount, 1, 'orders count is still the same since the parameter is not correct');
    })
  });

  it('only the manufacturer can add a chip', function () {
    return cryptoChip.deployed().then(function (instance) {
      CryptoChipInstance = instance;
      return CryptoChipInstance.addChip(1, [12, 123], ['0xdd87', '0xdd87'],
        '0xaaaa', '0xdddd', [false, false], accounts[4], accounts[3],
        ['0x4567', '0x7896'], '0xdddd', { from: accounts[1] });
    }).then(assert.fail).catch(function (error) {
      assert(error.message.indexOf('revert') >= 0, 'error message must contain revert');
      return CryptoChipInstance.chipsCount();
      }).then(function (chipsCount) {
      assert.equal(chipsCount, 0, 'only the manufacturer can add a chip');
    })
  });

  it('add a new chip', function () {
    return cryptoChip.deployed().then(function (instance) {
      CryptoChipInstance = instance;
      CryptoChipInstance.addChip(1, [12, 123], ['0xdd87', '0xdd87'],
        '0xaaaa', '0xdddd', [false, false], accounts[4], accounts[3],
        ['0x4567', '0x7896'], '0xdddd', { from: accounts[4] });
      return CryptoChipInstance.chipsCount();
    }).then(function (chipsCount) {
      assert.equal(chipsCount, 2, 'the chip has been added successfully by the manufacturer');
    })
  });

  it('throws an exception for invalid chip parameters', function () {
    return cryptoChip.deployed().then(function (instance) {
      CryptoChipInstance = instance;
      return CryptoChipInstance.addChip(1, [123], ['0xdd87'],
        '0xaaaa', '0xdddd', [false], '', '',
        ['0x4567'], '0xdddd', { from: accounts[4] });
    }).then(assert.fail).catch(function (error) {
      assert(error.message.indexOf('invalid address') >= 0, 'error message must contain (invalid address) phrase');
      return CryptoChipInstance.chipsCount();
    }).then(function (chipsCount) {
      assert.equal(chipsCount, 2, 'the chip has not been added due to parameters problem');
    })
  });

  it('only the owner of the token can set the chip counterfeit field', function () {
    return cryptoChip.deployed().then(function (instance) {
      CryptoChipInstance = instance;
      return CryptoChipInstance.setChipClonedField([12345], [true], accounts[2], accounts[1],
      ['0x4567'], { from: accounts[1] });
    }).then(assert.fail).catch(function (error) {
      assert(error.message.indexOf('revert') >= 0, 'error message must contain revert');
      return CryptoChipInstance.balanceOf(accounts[2]);
    }).then(function (balance) {
      assert.equal(balance, 0, 'you are not authorized to set the chip counterfeit field');
    })
  });

  it('set the chip counterfeit field', function () {
    return cryptoChip.deployed().then(function (instance) {
      CryptoChipInstance = instance;
      CryptoChipInstance.setChipClonedField([12], [false], accounts[2], accounts[1],
      ['0x4567'], { from: accounts[3] });
      return CryptoChipInstance.balanceOf(accounts[2]);
    }).then(function (balance) {
      assert.equal(balance, 1, 'the counterfeit filed has been sit successfully');
    })
  });

  it('only the owner of the token can sell a chip', function () {
    return cryptoChip.deployed().then(function (instance) {
      CryptoChipInstance = instance;
      return CryptoChipInstance.sellChip(accounts[1], '0x4567', { from: accounts[3] });
    }).then(assert.fail).catch(function (error) {
      assert(error.message.indexOf('revert') >= 0, 'error message must contain revert');
      return CryptoChipInstance.balanceOf(accounts[1]);
    }).then(function (balance) {
      assert.equal(balance, 0, 'you are not authorized to sell the chip');
    })
  });

  it('sell a chip', function () {
    return cryptoChip.deployed().then(function (instance) {
      CryptoChipInstance = instance;
      CryptoChipInstance.sellChip(accounts[1], '0x4567', { from: accounts[2] });
      return CryptoChipInstance.balanceOf(accounts[1]);
    }).then(function (balance) {
      assert.equal(balance, 1, 'you are not authorized to sell the chip');
    })
  });
})
